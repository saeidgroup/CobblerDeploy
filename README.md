# Deploying Cobbler VM

In the following we assume a clean install of Fedora (tested with 38 on a proxmox node). 

In the proxmox VM we have 3 NICs:
- `eth0`: used for outside traffic and acts as the gateway for the test proxmox VMs: 10.128.2.*
- `eth1`: on an SDN setup for testing cobbler using proxmox nodes: 172.25.1.
- `eth2`: on `vimbr0` in proxmox server which tags the traffic with 720.

## Deployment

1. Setup the system

- restart `NetworkManager-wait-online` service:

```
systemctl restart NetworkManager-wait-online.service
```

- fix nameserver in `resolv.conf`:
```
# remove the link
cp /etc/resolv.conf /tmp
rm /etc/resolv.conf
cp /tmp/resolv.conf /etc/resolv.conf

# change nameserver in /etc/resolv.conf
# nameserver 8.8.8.8

# restart systemd-resolved
systemctl restart systemd-resolved
```

- setup the OS:
```
dnf update -y
dnf install vim git ansible firewalld
```

- disable selinux:
```
# edit /etc/selinux/config and restart
SELINUX=disabled
```

- enable ip forwarding:
```
sysctl -w net.ipv4.ip_forward=1
# check the FORWARD rules of iptables
iptables -L -v -n
```

- setup masquerading:
```
ETHEXT=eth0
ETHINT=eth1

firewall-cmd --permanent --direct --add-rule ipv4 nat POSTROUTING 0 -o $ETHEXT -j MASQUERADE
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i $ETHINT -o $ETHEXT -j ACCEPT
firewall-cmd --permanent --direct --add-rule ipv4 filter FORWARD 0 -i $ETHEXT -o $ETHINT -m state --state RELATED,ESTABLISHED -j ACCEPT
```

- setup gateway:
```
nmcli con mod "Wired connection 1" ipv4.addresses 172.25.1.100/16
nmcli con mod "Wired connection 1" ipv4.gateway 172.25.0.1
nmcli con mod "Wired connection 1" ipv4.dns "8.8.8.8 8.8.4.4"
nmcli con mod "Wired connection 1" ipv4.method manual
nmcli con down "Wired connection 1"
nmcli con up "Wired connection 1"
```

- setup the second NIC
```
# this NIC will be used for tagged traffic
nmcli con mod "Wired connection 2" ipv4.addresses 172.26.1.100/16
nmcli con mod "Wired connection 2" ipv4.method manual
nmcli con down "Wired connection 2"
nmcli con up "Wired connection 2"
```

2. Installing Cobbler

- clone the `AnsibleRoleCobbler` repo:
```
git clone git@gitlab.com:saeidgroup/AnsibleRoleCobbler.git
```

- set the appropriate vars in `vars/main.yml` and run the `converge.yml` playbook against the VM.

3. Setup Cobbler

- clone the `AnsibleRoleCobblerManage` repo:
```
git clone git@gitlab.com:saeidgroup/AnsibleRoleCobblerManage.git
```

- set the appropriate credentials of the cobbler server in `var/main.yml` and run the `converge.yml` playbook against the VM.

4. Useful Cobbler commands:
```
# list distros, profiles, systems
cobbler distro|profile|system list
# add a system, see vars with --help 
cobbler system add --name=<name> <various options>
# see report about an object
cobbler distro|profile|system report --name=<name>
```
